﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projekt
{
    public partial class LoginForm : Form
    {
        public LoginForm()
        {
            InitializeComponent();
        }

        private void LoginButtonClick(object sender, EventArgs e)
        {
            string LLoginInputText = LLoginInput.Text;
            string LPasswordInputText = LPasswordInput.Text;

            string loginParams = LLoginInput + " " + LPasswordInput;

            string path = "users.txt";

            if (File.Exists(path))
            {
                string[] fileText = File.ReadAllLines(path);

                foreach (string line in fileText)
                {
                    if (line == loginParams)
                    {
                        //RedirectToMain(LLoginInput);
                    }
                    else
                    {
                        //ErrorBox.Text = "Nieprawidłowy id lub hasło. Spróbuj ponownie!";
                    }
                }
            }
            else
            {
                //ErrorBox.Text = "Baza z użytkownikami jest pusta. Zarejestruj się!";
            }

        }
        private void RedirectToRegisterButtonClick(object sender, EventArgs e)
        {
            RedirectToRegister();
        }
        private void RedirectToRegister()
        {
            RegisterForm registerForm = new RegisterForm
            {
                Width = this.Width,
                Height = this.Height,
                StartPosition = FormStartPosition.Manual,
                Location = new Point(this.Location.X, this.Location.Y)
            };
            this.Hide();
            registerForm.ShowDialog();
            this.Close();
        }

    }

}


