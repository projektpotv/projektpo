﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projekt
{
    public partial class RegisterForm : Form
    {
        public RegisterForm()
        {
            InitializeComponent();
        }

        private void RegisterButtonClick(object sender, EventArgs e)
        {
            string RLoginInputText = RLoginInput.Text;
            string RPasswordInputText = RPasswordInput.Text;

            string newUser = RLoginInputText + " " + RPasswordInputText + Environment.NewLine;



            string path = "users.txt";

            File.AppendAllText(path, newUser);

        }

        private void RedirectToLoginButtonClick(object sender, EventArgs e)
        {
            RedirectToLogin();
        }
        private void RedirectToLogin()
        {
            LoginForm loginForm = new LoginForm
            {
                Width = this.Width,
                Height = this.Height,
                StartPosition = FormStartPosition.Manual,
                Location = new Point(this.Location.X, this.Location.Y)
            };
            this.Hide();
            loginForm.ShowDialog();
            this.Close();
        }

    }
}
